from .context import onavdata

import unittest
import numpy as np


class BasicTestSuite(unittest.TestCase):
    """Basic test cases."""

    def test_get_data_noarg(self):
        df = onavdata.get_data()
        assert len(df) > 10

    def test_print_shortnames(self):
        onavdata.print_shortnames()

    def test_Rsensor2body_eye(self):
        """
        Verify that defining a Rsensor2body as eye(3) does not change
        the original data.
        """
        s = 'SIM-MEASURED-CAR NORTH FIXED-SPEED FIXED-HEADING'
        df = onavdata.get_data(s)

        row = df.iloc[0]
        # Quantities copied over from the shortname above.:
        row_expect = {'AccelX (m/s^2)': 0.983058639970182,
                      'AccelY (m/s^2)': 0.976889892557545,
                      'AccelZ (m/s^2)': -9.33035802475368,
                      'AngleRateX (rad/s)': 0.008715674381091,
                      'AngleRateY (rad/s)': 0.00436806470867,
                      'AngleRateZ (rad/s)': 0.002644128942314,
                      'MagFieldX (G)': 0.383085,
                      'MagFieldY (G)': 0.122257,
                      'MagFieldZ (G)': 0.425408}
        for col in row_expect:
            self.assertAlmostEqual(row_expect[col], row[col])

    def test_dtype_override(self):
        """
        Verify that dtype for simulated perfect data is double.  The original
        data has columns of integers which are loaded as int64 by default.
        data has columns of integers which are loaded as int64 by default.
        This test verifies that meta can be used to enforce the datatype
        desired.
        """
        s = 'SIM-PERFECT-CAR NORTH FIXED-SPEED FIXED-HEADING'
        df = onavdata.get_data(s)
        # Expected dtypes for subset of columns
        cols_dtype_dict = {
        'AccelX (m/s^2)': 'float64',
        'AngleRateX (rad/s)': 'float64',
        'VelNorth (m/s)': 'float64',
        'PosNorth (m)': 'float64',
        'PosNorth (m)': 'float64',
        'GNSSVelNorth (m/s)': 'float64',
        'GNSSVelNorth (m/s)': 'float64',
        'GNSSPosNorth (m)': 'float64',
        'GNSSPosNorth (m)': 'float64',
        'CompassAngleHeading (rad)': 'float64',
        'CompassAngleHeading (rad)': 'float64',
        'AngleHeading (rad)': 'float64',
        'AngleHeading (rad)': 'float64',
        'MagFieldX (G)': 'float64',
        }
        for col in cols_dtype_dict:
            self.assertEqual(df[col].dtype, cols_dtype_dict[col])

    def test_add_dataset(self):
        """
        Test adding dataset by loading one from `sample_project_datasets`
        folder.
        """
        # Add a new dataset and load one.
        shortname = ('Simulated Square Path with Variable Pitch and Zero Roll'
                     ' - Mid-Accuracy Measurements')
        onavdata.add_dataset(name='my_project_datasets',
                             path='examples/sample_project_datasets')

        df = onavdata.get_data(shortname)
        assert len(df) > 10

    def test_load_endaq_dataset(self):
        """
        Test loading an example .IDE dataset logged from enDAQ device.
        """
        shortname = 'Example enDAQ IDE File - Sequence of Level-Pitch-Level x5'
        onavdata.add_dataset(name='my_project_datasets',
                             path='examples/sample_project_datasets')

        df = onavdata.get_data(shortname)
        assert len(df) > 10

    def test_load_dataset_keep_rows_gt(self):
        """
        Check that sample dataset, which leverages `keep-rows-gt` in meta,
        properly filters data upon loading.
        """
        shortname = ('(t > 5 sec) Simulated Square Path with Variable Pitch '
                     'and Zero Roll - Mid-Accuracy Measurements')
        onavdata.add_dataset(name='my_project_datasets',
                             path='examples/sample_project_datasets')

        df = onavdata.get_data(shortname)
        assert (df.index > 5).all()

    def test_utils_convert_rad2deg(self):
        """
        Check for proper unit conversion and handling of `inplace` option.
        """
        shortname = '2021-04-27 HGuide n380 Car Drive Between Traffic Lights'
        col = 'AngleRoll (rad)'
        col_deg = col.replace('(rad)', '(deg)')
        df = onavdata.get_data(shortname)

        assert col in df

        # Note, conversion is applied to entire dataframe but we only check
        # one column for this test.
        df_deg = onavdata.utils.convert_rad2deg(df)
        assert col_deg in df_deg
        np.testing.assert_array_equal(df_deg[col_deg], np.rad2deg(df[col]))

        # Now apply in-place conversion.
        onavdata.utils.convert_rad2deg(df, inplace=True)
        assert col_deg in df
        assert col not in df
        np.testing.assert_array_equal(df_deg[col_deg], df[col_deg])

    def test_utils_convert_mps2_to_G(self):
        """
        Check for proper unit conversion - picks dataset with little dynamics
        so accelration magnitude should be approximately  1 G.

        Also demonstrates usage of getting sensor columns
        """
        shortname = 'SIM-MEASURED-CAR NORTH FIXED-SPEED FIXED-HEADING'
        df = onavdata.get_data(shortname)
        cols = onavdata.utils.get_accel_cols(df)

        assert '(m/s^2)' in cols[0]
        # Note: optionally the magnitude of G in m/s^2 can be
        #       passed in for the conversion.  Here we use default.
        onavdata.utils.convert_mps2_to_G(df, inplace=True)
        # Update accelerometer columns and check for change in units.
        cols = onavdata.utils.get_accel_cols(df)
        assert '(m/s^2)' not in cols[0]
        assert '(G)' in cols[0]

        # Only check to one decimal place, since "G" to "m/s^2" conversion
        # done using default values.
        np.testing.assert_array_almost_equal((df[cols]**2).sum(axis=1)**0.5,
                                              np.ones(len(df[cols])),
                                              decimal=1)


    def test_utils_convert_G_to_mps2(self):
        """
        Check for proper unit conversion - picks dataset with little dynamics
        so accelration magnitude should be approximately  9.81 G.

        Also demonstrates usage of getting sensor columns
        """
        # One of the sample datasets has units of G for accelerometer.
        # Note that there are rows with NANs as well, but they are handled.
        onavdata.add_dataset(name='my_project_datasets',
                             path='examples/sample_project_datasets')
        shortname = 'Example enDAQ IDE File - Sequence of Level-Pitch-Level x5'
        df = onavdata.get_data(shortname)
        cols = onavdata.utils.get_accel_cols(df)
        assert '(G)' in cols[0]
        df_mps2 = onavdata.utils.convert_G2mps2(df)

        # Update accelerometer columns and check for change in units.
        cols_mps2 = onavdata.utils.get_accel_cols(df_mps2)
        assert '(m/s^2)' in cols_mps2[0]

        # Only check to one decimal place, since "G" to "m/s^2" conversion
        # done using default values.
        np.testing.assert_array_almost_equal(df[cols]*9.81,
                                             df_mps2[cols_mps2], decimal=1)


if __name__ == '__main__':
    unittest.main()
