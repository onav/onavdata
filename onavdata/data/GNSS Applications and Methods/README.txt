This data was generated using software from the book:

GNSS Applications and Methods
Demoz Gebre-Egziabher, Scott Gleason
Copyright 2009

More specifically, Case Study 2 from Chapter 7.  Slight modifications were made
to the software adding the vertical channel to the simulation.  Magnetometer 
data was simulated separately and added to the data set.

The script source code in not included here (refer to the book).  But the
original README file (readme_case_2.pdf) and script license (bsd.txt) are
included in the data directory (Chapter 7 - Case Study 2 - Add Mag) for
reference.

## Measurement Errors

Details of the measurement errors simulated are given below.

### Accelerometer
```
  %   (e.1)   Generate Null Shift
    b_asx = 0.1*g;           %   x-accelerometer null shift (m/s/s)
    b_asy = 0.1*g;           %   y-accelerometer null shift (m/s/s)
    b_asz = 0.05*g;          %   z-accelerometer null shift (m/s/s)
    
    %   (e.2)   Define Correlated Bias Parameters
    tau_a = 300;                %   Correlation time for accelerometer errors

    sigma_ad = 0.001*g;        %   Standard deviation of accelerometer 
                              %   correlated bias
    b_adx = markov_noise(sigma_ad,tau_a,dt,t(end)); 
    b_ady = markov_noise(sigma_ad,tau_a,dt,t(end)); 
    b_adz = markov_noise(sigma_ad,tau_a,dt,t(end)); 

    %   (e.3)   Define Wide Band Noise Parameters
    sigma_wa = 0.001*g;         %   Accelerometer output noise standard 
                                %   deviation
    w_ax = sigma_wa*randn(drl,1);                                                        
    w_ay = sigma_wa*randn(drl,1);
    w_az = sigma_wa*randn(drl,1);

    %   (e.4)   Combined Sensor Errors
    b_ax = b_asx + b_adx + w_ax;
    b_ay = b_asy + b_ady + w_ay;
    b_az = b_asz + b_adz + w_az;

    %   (e.4)   Generate Inertial Sensor Outputs
    f_x = xddot + b_ax;
    f_y = yddot + b_ay;
    f_z = zddot + b_az;
```

### Gyroscope
```
    %   (e.1)   Generate Null Shift
    b_gsx = 0.5*d2r;          %   x-gyro null shift (radians/sec).
    b_gsy = 0.25*d2r;         %   y-gyro null shift (radians/sec).
    b_gsz = 0.15*d2r;         %   z-gyro null shift (radians/sec).

    %   (e.2)   Define Correlated Bias Parameters
    tau_g = 300;                %   Correlation time for gyro errors
    sigma_gd = 0.05*d2r;        %   Standard deviatio of (x,y,z)-gyro correlated bias
    b_gdx = markov_noise(sigma_gd,tau_g,dt,t(end));
    b_gdy = markov_noise(sigma_gd,tau_g,dt,t(end));
    b_gdz = markov_noise(sigma_gd,tau_g,dt,t(end));

    %   (e.3)   Define Wide Band Noise Parameters                        
    sigma_wg = 0.001*d2r;       %   Rate gyro output noise standard deviation
    w_gx = sigma_wg*randn(drl,1);                                                        
    w_gy = sigma_wg*randn(drl,1);  
    w_gz = sigma_wg*randn(drl,1);  

    %   (e.4)   Combined Sensor Errors
    b_gx = b_gsx + b_gdx + w_gx;
    b_gy = b_gsy + b_gdy + w_gy;
    b_gz = b_gsz + b_gdz + w_gz;

    %   (e.4)   Generate Inertial Sensor Outputs
    omega_x = wx + b_gx;
    omega_y = wy + b_gy;
    omega_z = psidot + b_gz;
```

### Compass
```
    %   Note:  The magnetometer/compass errors are assumed to be uncorrelated
    %   white noise sequences.  This is a gross idealization.  Realistic error
    %   models for these sensors include several correlated terms.
    compass_noise = 0.25*d2r;       %   Standard deviation of compass noise
    psi_compass = psi_true + compass_noise*randn(drl,1);
```

### GNSS
```
    %   Note:  The GNSS errors here are assumd to be uncorrelated.  For the level
    %   accuracy we are dealing with here this is acceptable. 
    GNSS_vel_noise = 0.1;           %   m/s
    GNSS_pos_noise = 1.0;           %   m

    v_N_GNSS = v_N + GNSS_vel_noise*randn(drl,1);
    v_E_GNSS = v_E + GNSS_vel_noise*randn(drl,1);
    v_D_GNSS = v_D + GNSS_vel_noise*randn(drl,1);

    p_N_GNSS = p_N + GNSS_pos_noise*randn(drl,1);
    p_E_GNSS = p_E + GNSS_pos_noise*randn(drl,1);
    p_D_GNSS = p_D + GNSS_pos_noise*randn(drl,1);
```

### Magnetometer
```python
    # Reference: http://www.ngdc.noaa.gov/geomag/magfield.shtml
    nT2G = 1e-5# conversion from nanoTesla to Gauss
    h = np.array([17491.9, 129.3, 52886.7])*nT2G # [Guass] for Zip Code: 55455 (U of MN)

    # Load simulated sensor errors for magnetometer & form
    # sigma_n - standard deviation of additive Guassian white-noise
    # b - 3x1 vector, null-shift or hard iron errors
    # Cs - 3x3 matrix, scale factor
    # Ce - 3x3 matrix, misalignment affects
    # Ca - 3x3 matrix, soft-iron and axes nonorthogonality
    #
    # C = Cs * Ce * Ca
    sigma_n = 0.005  # Guass # TODO: decide whether to add noise term or not

    bx, by, bz = 0.2, 0.12, -0.1 # bias
    sx, sy, sz = 0.0, 0.0, 0.0   # scale factor (0: no scale factor error)
    ex, ey, ez = 0., 0., 0.      # misalignment errors (0: no misalignment error)

    # Combined soft-iron and axes nonorthogonality effects (0: no error)
    axx, ayy, azz = 0., 0., 0.

    # Assume symmetric effects.
    axy = ayx = 0.
    axz = azx = 0.
    ayz = azy = 0.

    # Form error terms
    Cs = np.eye(3) + np.diag([sx, sy, sz])
    Ce = np.array([[ 1.,  ez, -ey],
                   [-ez,  1.,  ex],
                   [ ey, -ex,  1.]])
    Ca = np.array([[1.+axx,   axy,   axz],
                   [  ayx, 1.+ayy,   ayz],
                   [  azx,   azy, 1.+azz]])
    b = np.array([bx, by, bz])
    n = sigma_n * np.random.randn(drl, 3) 

    C = Cs.dot(Ce).dot(Ca)

    # Corrupt true body-frame measurements according to:
    # hm = C * hb + b + n
    # where `hb` is the body-frame representation of the local NED
    # magnetic field `h`.
    hm = C.dot(hb.T).T + b + n
```
