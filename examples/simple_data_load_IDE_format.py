"""Example script loading enDAQ device IDE sample data"""

import onavdata
from onavdata.utils import get_accel_cols

if __name__ == '__main__':

    # Add a new dataset
    onavdata.add_dataset(name='my_project_datasets',
                         path='examples/sample_project_datasets')

    # Get project dataset
    shortname = ('Example enDAQ IDE File - Sequence of Level-Pitch-Level x5')
    df = onavdata.get_data(shortname)

    # Check that initial transients have been trimmed via meta parameter.
    # Note that there are rows with NANs as well, hence the `dropna()`.
    print(df[get_accel_cols(df)].dropna().head(20))

    print(df.info())
