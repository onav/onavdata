"""Example script leveraging dataframe utils"""

from onavdata.utils import convert_mG2mps2, convert_deg2rad
import pandas as pd

if __name__ == '__main__':
    df = pd.DataFrame({'TimeFromStart (s)': [0.0, 1, 2],
                       'AccelX (mG)': [0, 0, 0],
                       'AccelY (mG)': [0, 0, 0],
                       'AccelZ (mG)': [0, 0, 1000],
                       'AngleHeading (deg)': [0, 90, 180],
                       'AnglePitch (deg)': [0, 0, 0],
                       'AngleRoll (deg)': [0, 0, 0],
                       'AngleRateX (deg/s)': [0, 0, 0],
                       'AngleRateY (deg/s)': [0, 0, 0],
                       'AngleRateZ (deg/s)': [90, 90, 0]})
    print("Pre-Conversion:")
    print(df)

    convert_mG2mps2(df, inplace=True)
    convert_deg2rad(df, inplace=True)

    print("Post-Conversion:")
    print(df)
