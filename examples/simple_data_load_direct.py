"""Example of loading dataset by directly supplying
   the file path and meta dictionary."""
import toml
from collections import OrderedDict
import onavdata
import os
from onavdata.utils import get_accel_cols

if __name__ == '__main__':
    # Add a new dataset
    path_dataset = os.path.join('examples',
                                'sample_project_datasets',
                                'onavdata-square-vari-pitch-zero-roll-mid-accuracy-measurements',
                                'square-vari-pitch-zero-roll-mid-accuracy-measurements.csv')
    path_meta = os.path.join('examples',
                             'sample_project_datasets',
                             'onavdata-square-vari-pitch-zero-roll-mid-accuracy-measurements',
                             'meta.toml')
 
    meta = toml.load(path_meta, _dict=OrderedDict)
    df = onavdata.get_data_direct(path_dataset, meta)

    # This should match with the other method for loading data
    # as shown below in `examples/simple_data_load.py`.

