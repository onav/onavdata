"""Example script loading sample data"""

import onavdata
from onavdata.utils import get_accel_cols

if __name__ == '__main__':
    # Print available shortnames
    onavdata.print_shortnames()

    shortnames = [
                  'SIM-PERFECT-CAR NORTH FIXED-SPEED FIXED-HEADING',
                  'SIM-MEASURED-CAR NORTH FIXED-SPEED FIXED-HEADING',
                  '2011 UMN-UAV GPSFASER1',
                  '2021-04-27 HGuide n380 Car Hill Driving'
                  ]
    # Loop through list of shortnames and load/print first 5 lines.
    for shortname in shortnames:
        print("="*5)
        print(shortname)
        df = onavdata.get_data(shortname)
        print(df.head(5))

    # Print accelerometer columns:
    cols_accel = get_accel_cols(df)
    print("Accel Columns in '%s'" % shortname)
    print(cols_accel)

    # Add a new dataset
    onavdata.add_dataset(name='my_project_datasets',
                         path='examples/sample_project_datasets')
    # Check if desired datasets are added
    onavdata.print_shortnames()

    # Get project dataset
    shortname = ('Simulated Square Path with Variable Pitch and Zero Roll - '
                 'Mid-Accuracy Measurements')
    df = onavdata.get_data(shortname)
    print(df.head(5))

    # Show added column
    print("See example of column added via meta `add-columns` entry.")
    print(df.columns)

    # Show adding column from another column
    print("See example of defining a new column based on another")
    print(df['ExampleAddColumnCopy'])
