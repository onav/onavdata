"""Example script loading dataset trimmed via meta file
"""

import onavdata

if __name__ == '__main__':
    # Add a new dataset
    onavdata.add_dataset(name='my_project_datasets',
                         path='examples/sample_project_datasets')
    # Check if desired datasets are added
    onavdata.print_shortnames()

    # Get dataset and check for proper trimming
    shortname = '(t > 5 sec) Simulated Square Path with Variable Pitch and Zero Roll - Mid-Accuracy Measurements'
    df = onavdata.get_data(shortname)
    print(df)
