"""Example script loading properties from dataset"""

import onavdata
from onavdata.utils import get_accel_cols

if __name__ == '__main__':
    # Print available shortnames
    onavdata.print_shortnames()

    shortnames = [
                  'SIM-PERFECT-CAR NORTH FIXED-SPEED VARY-HEADING',
                  '2012 UMN-UAV THOR79',
                  '2021-03-29 HGuide n380 Car Drive Between Traffic Lights'
                  ]
    # Loop through list of shortnames and load/print first 5 lines.
    for shortname in shortnames:
        print("="*5)
        print(shortname)
        df = onavdata.get_data(shortname)
        print(df.head(5))

        desc = onavdata.get_description(shortname)
        print(desc)
        media = onavdata.get_media(shortname)
        print(media)



