"""Example script loading dataset with multiple files
"""

import onavdata

if __name__ == '__main__':
    # Add a new dataset
    onavdata.add_dataset(name='my_project_datasets',
                         path='examples/sample_project_datasets')
    # Check if desired datasets are added
    onavdata.print_shortnames()

    # Get project dataset
    shortname = 'Example Multiple File Dataset'
    df = onavdata.get_data(shortname)
    print(df)
