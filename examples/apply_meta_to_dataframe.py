"""Example of programmatically applying meta to loaded dataframe.
"""
from collections import OrderedDict
import onavdata
import os
import pandas as pd
import toml

if __name__ == '__main__':
    shortnames = [
                  'SIM-PERFECT-CAR NORTH FIXED-SPEED FIXED-HEADING',
                  'SIM-MEASURED-CAR NORTH FIXED-SPEED FIXED-HEADING',
                  '2011 UMN-UAV GPSFASER1',
                  ]
    meta = {'columns-use':['TimeFromStart (s)',
                           'AccelX (m/s^2)',
                           'AccelY (m/s^2)',
                           'AccelZ (m/s^2)'],
            'columns-round-decimals':{
                           'AccelZ (m/s^2)':1
                           }
            }
    # Loop through list of shortnames and load/print first 5 lines.
    for shortname in shortnames:
        print("="*5)
        print(shortname)
        df_original = onavdata.get_data(shortname)
        print(df_original.head(5))
        
        # It should work with/without original index.      
        df = onavdata.apply_meta(df_original.reset_index(drop=False), meta)
        df = onavdata.apply_meta(df_original, meta)
        print(df.head(5))
        
        # Repeat loading shouldn't change anything.
        pd.testing.assert_frame_equal(df, onavdata.apply_meta(df, meta))


